﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using System;

public class Cell: MonoBehaviour
{

    public int x;
    public int y;
    public bool isFlagged = false;
    public bool isRevealed = false;
    public bool isMine;
    public Texture[] Textures;
    public Texture mineTexture;
    public Texture defaultTexture;
    public Texture flagTexture;
    private RawImage rawImage;
    private Texture texture;

    public int AroundMines {get; internal set;}
    void Start()
    {
        rawImage = GetComponent<RawImage>();   
    }

    // Update is called once per frame
    void Update()
    {
    
    }

     private void OnMouseOver() {
         if(Input.GetMouseButtonUp(0)){
            Game.Reveal(x,y);
            this.isRevealed = true;   
             loadTexture();  
         }else if(Input.GetMouseButtonUp(1))
         {
            Game.Flag(x,y);
            loadTexture();
              
         }  
         
     }

     public void loadTexture() {
        if(!isRevealed && isFlagged)
        {
            rawImage.texture = flagTexture;
        }
        else if (isRevealed && isMine && !isFlagged)
            rawImage.texture = mineTexture;
        else if(isRevealed && isMine && isFlagged){
            rawImage.texture = flagTexture;
        }
        else if(isRevealed && !isMine)
            rawImage.texture = Textures[AroundMines];
        else
        {
            rawImage.texture = defaultTexture;
        }
    }

    public void Open(){
        
        isRevealed = true;
        if(isMine && !isFlagged){
             rawImage.texture = mineTexture;
        }
        else if(isMine && isFlagged){
            rawImage.texture = flagTexture;
        }
        else
        {
            rawImage.texture = Textures[AroundMines];
        }
    }

}

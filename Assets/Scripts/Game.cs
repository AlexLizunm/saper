﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public Text numOfRows;
    public Text numOfColumns;
    public Text numOfMines;
    public RectTransform panelRow;
    public Cell gridCell;   
    public Transform grid;
    static Animator anim;
    private static  bool isFirstReveal;    
    private static int rowSize;
    private static  int columnSize;
    private static  int minesPercent;
    private static int mines;
    private static Cell[,] cells;
    
    void Start()
    {
       anim = GetComponent<Animator>();

    }
    // Update is called once per frame
    void Update()
    {
        
    }
   
   public void OnGenerateGrid(){
       
       Initialize();
       ClearGrid();
       Cell cellInputField;
       
       RectTransform rowParent;
        for (int rowIndex = 0; rowIndex < rowSize; rowIndex++)
        {
            rowParent = (RectTransform)Instantiate(panelRow);
            rowParent.transform.SetParent(grid);
            rowParent.transform.localScale = Vector3.one;
            for (int colIndex = 0; colIndex < columnSize; colIndex++)
            {
                cellInputField = (Cell) Instantiate(gridCell);
                cellInputField.transform.SetParent(rowParent);
                cellInputField.GetComponent<Transform>().localScale = Vector3.one;
                cells[colIndex, rowIndex] = cellInputField;
                cellInputField.x = colIndex;
                cellInputField.y = rowIndex;
            }
        }
        isFirstReveal = true;
   }
        
    void Initialize()
    {
        rowSize = int.Parse(numOfRows.text);
        columnSize = int.Parse(numOfColumns.text); 
        cells = new Cell[columnSize,rowSize];
        minesPercent = int.Parse(numOfMines.text);
        mines = (int)(minesPercent*cells.Length/100);

    }
    void ClearGrid()
    {
        for (int count = 0; count < grid.childCount; count++)
        {
            Destroy(grid.GetChild(count).gameObject);
        }
        anim.SetBool("Win", false);
        anim.SetBool("Lose", false);
    }
        

    public static void Reveal(int x, int y){

        if (isFirstReveal)
        {
            isFirstReveal = false;
            
            PopulateGrid(x,y);
        }
        if(cells[x,y].isMine){

            GameOver();
        }

        if(cells[x,y].AroundMines == 0){

            RevealEmpty(x,y);

        }
       
    }
//In unity editor maybe works slow if the mines amount is too small so safe area is huge, in compiled app all right
    private static void RevealEmpty(int x, int y){
        cells[x,y].Open();
        for (int nx = x - 1; nx <= x + 1; nx++)
            {
                if (nx < 0 || nx == columnSize)
                    continue;

                for (int ny = y - 1; ny <= y + 1; ny++)
                {
                    if (ny < 0 || ny == rowSize)
                        continue;

                    if (nx == x&& ny == y)
                        continue;
                    if(cells[nx,ny].isRevealed){
                        continue;
                    }
                    if(cells[nx,ny].AroundMines == 0 ){
                         RevealEmpty(nx,ny);
                    }
                   cells[nx,ny].Open();
                }
            }
    }
    private static void GameOver(){

        for(int x = 0; x < columnSize; x++){

            for(int y = 0; y < rowSize; y++){

                cells[x,y].Open();
            }
        }
        anim.SetBool("Lose", true);
    }

     private static bool GameWin(){

       for(int x = 0; x < columnSize; x++){

            for(int y = 0; y < rowSize; y++){

            if ((cells[x,y].isMine != cells[x,y].isFlagged))
                return false;
            }
       }
       return true;
    }
    public static void Flag(int RightClickX, int RightClickY){
        cells[RightClickX,RightClickY].isFlagged =!cells[RightClickX,RightClickY].isFlagged;
       if(GameWin()){
           for(int x = 0; x < columnSize; x++){

                for(int y = 0; y < rowSize; y++){
                    if(!cells[x,y].isRevealed){
                        cells[x,y].Open();
                    }
                }
           }
            anim.SetBool("Win", true);
        }

    }
    private static void PopulateGrid(int firstClickX, int firstClickY){
         
        var random = new System.Random();
        var placed = 0;

        while (placed < mines)
        {
            var x = random.Next(columnSize);
            var y = random.Next(rowSize);

            if ((x != firstClickX && y != firstClickY) &&!cells[x, y].isMine)
            {
                cells[x, y].isMine = true;
                placed++;
            }
        }

         for (int x = 0; x < columnSize; x++)
        {
            for (int y = 0; y < rowSize; y++)
            {
                var cell = cells[x, y];
                if (cell.isMine)
                    continue;
                cell.AroundMines = CountAroundMines(x,y);
            }
        }

    }

    private static int CountAroundMines(int x ,int y)
        {
            var count = 0;

            for (int nx = x - 1; nx <= x + 1; nx++)
            {
                if (nx < 0 || nx == columnSize)
                    continue;

                for (int ny = y - 1; ny <= y + 1; ny++)
                {
                    if (ny < 0 || ny == rowSize)
                        continue;

                    if (nx == x&& ny == y)
                        continue;
                    if(cells[nx,ny].isMine)
                         count++;

                   
                }
            }
            return count;
        }
}
